var gulp = require('gulp');
var jade = require('gulp-jade');
var sass = require('gulp-ruby-sass');
var webserver = require('gulp-webserver');

gulp.task('webserver', function(){
		gulp.src('dist')
			.pipe(webserver({
							livereload: true,
								port: 8039,
								open: true
						}));
});

gulp.task('css', () =>
    sass('./app/sass/*.scss')
        .on('error', sass.logError)
        .pipe(gulp.dest('./dist/css/'))
);
 
gulp.task('html', function() {
  gulp.src('./app/*.jade')
    .pipe(jade({}))
    .pipe(gulp.dest('./dist/'))
});

gulp.task('watch', function(){
		gulp.watch(['./app/sass/**/*.scss'], ['css'] );
		gulp.watch(['./app/index.jade'], ['html'] );

});
gulp.task('default', [ 'html', 'css', 'webserver', 'watch' ]);
